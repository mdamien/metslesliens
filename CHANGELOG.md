La librairie `metslesliens` suit un [versionnement sémantique](https://semver.org/lang/fr/).

## 1.1.1 (2018-05-06)

### Corrigé
- Fichiers de grammaire non-lu lorsque l’import se fait depuis un autre programme Python via sys.path


## 1.1.0 (2018-05-01)

### Ajouté
- Format de sortie dédié au debug et notamment à la recherche de faux négatifs par l’affichage de contexte autour des expressions reconnues ou traitées
- Dérivation possible de la grammaire en des grammaires "dégradées" automatiquement, pour l’instant avec des accents incorrects ou l’expression erronée "code du commerce"
- Reconnaissance des liens généraux vers des textes sans mention d’un article spécifique
- Reconnaissance des divisions supra-articles "(sous-)?section", "(sous-)?paragraphe", "partie"
- Reconnaissance des chaînes de divisions supra-articles, par exemple "chapitre X du titre VII du livre VII du code de justice administrative"
- Date optionnelle pour les textes européens

### Mofifié
- Ajout des pré-candidats "la", "l['’]", "section", "partie" pour la reconnaissance des sections et lois isolées


## 1.0.1 (2018-04-29)

### Corrigé
- Nature "livre/titre/chapitre" manquante dans le format structuré


## 1.0.0 (2018-04-28)

### Ajouté
- Paramètre --format=texte|structuré|arbre ajouté au programme principal
- Ajout de la reconnaissance des chapitres, titres et livres à un niveau similaire aux articles, mis à part que ces types ne peuvent être précisés par un alinéa
- Ajout de la reconnaissance d’expressions comme « au présent article » et « article ci-après » (idem pour les chapitres, titres, livres)
- Afin de faciliter la lecture de l’arbre résultat, notamment pour le format structuré, une grammaire rudimentaire de parcours dans l’arbre est créée, et plusieurs règles sont énoncées dans cette grammaire, par exemple la lecture du nom du texte dans un lien
- Ajout d’une fonction générateur, la fonction retournant une liste reste disponible et énumère simplement le générateur
- La fonction générateur retourne le format structuré par défaut, alors que la fonction liste retourne le format texte par défaut
- Reconnaissance de l’expression « dudit code » (ou autre texte)
- Reconnaissance des expressions pour les articles relatifs : « à l’article précédent », « au précédent article » (idem pour « suivant », « ci-avant », « ci-après »)
- Reconnaissance des directives et règlements européens (version préliminaire)
- Reconnaissance des expressions « audit premier alinéa » et « au même premier alinéa »

### Modifié
- La valeur du paramètre format=chaîne est renommée en format=texte
- Regroupement des quatres règles d’entrée de la grammaire en une seule, cela réduit fortement la complexité de la bibliothèque, permet de mieux appréhender les expressions capturées dans la rédaction de la grammaire et simplifie les données retournées unifiées en une seule entité
- La lecture des grammaires se fait de façon interne et « statique » (une seule fois)


## 0.3.0 (2018-04-15)

### Modifié
- Regroupement des quatre grammaires en une unique grammaire, il faut désormais utiliser les différents points d’entrée
- Meilleure reconnaissance de la liaison alinéa-article par une règle de grammaire dédiée
- Optimisation de la performance et de la robustesse de la grammaire par l’utilisation d’expressions régulières sans prise en compte de la casse
- Dans la fonction `donnelescandidats`, l’index retourné était celui du mot "article" ou de l’alinéa isolé, c’est désormais toujours le début de l’expression entière reconnue, y compris l’alinéa et la liaison éventuels
- La fonction `donneslescandidats` ne prend désormais en arguments que le texte et le format de sortie demandé (chaîne, structuré ou arbre)

### Corrigé
- Réduction du nombre de faux négatifs dans la reconnaissance d’alinéas d’article du fait de la liaison propre alinéa-article
- Les alinéas isolés du dernier bloc "pré-candidat d’article" n’étaient pas recherchés
- Dans le cas des alinéas isolés, le programme principal avaient des index décalés du nombre de caractères de l’expression reconnue
- Expressions insensibles à la casse pour la majorité de la grammaire


## 0.2.0 (2018-04-08)

### Ajouté
- Reconnaissance des noms d’alinéas, rattachés à un article ou isolés


## 0.1.0 (2018-04-07)

### Ajouté
- Reconnaissance des noms d’articles (sauf quelques exceptions)
