Notes légistiques
=================

Noms des articles
-----------------

Afin de vérifier la qualité de la règle sur les noms d’articles (règle "article" de `grammaire-liens.txt`, ainsi que les sous-règles "naturearticle", "numarticle", "cies"), voici quelques statistiques faites à partir de la base de données [legi.py](https://github.com/Legilibre/legi.py).

Bien noter que seuls les noms d’articles sont étudiés ici, pas les populations d’articles : il y a probablement plusieurs articles appelés « L111-1 », mais seul le nom nous importe ici. À titre d’information, il y a 1 355 165 articles dans le corpus étudié.

La requête SQL d’extraction des noms d’articles est :
```sql
SELECT DISTINCT num FROM articles ORDER BY num;
```

Les statistiques suivantes sont faites sur la livraison 20180323-200507 de la base LEGI. Les outils utilisés sont les commandes GNU/Linux `egrep` et `wc`, ce sont donc des expressions régulières classiques. À partir des types de numérotations « compliqués », il faudrait compléter cette étude en faisant tourner la grammaire du programme.

### Statistiques générales

* 143 767 numéros distincts d’articles
* 167 834 mots

### Statistiques par type de numérotation « simples »

Noter que certains articles n’ont pas de numéro, ils valent NULL dans la base legi.py. Il y a aussi le numéro constitué seulement d’une espace typographique dans la base legi.py (et dans LEGI ?). Dans la première catégorie ci-dessous, 15 numéros d’articles existent aussi dans une version suivie d’un retour à la ligne. Les chiffres sont donc diminués de 15 unités par rapport au chiffre brut lors qu’ils relèvent de la première catégorie, et le total ci-dessus est diminué de 16 unités par rapport au chiffre brut.

* seulement des chiffres `^[0-9]+$` : 2540 numéros différents allant de 1 à 2534, noter qu’il y a des numéros 01 à 06
* seulement des chiffres ou des tirets `^[0-9-]+$` : 12 441 numéros différents
* seulement des chiffres ou des tirets ou des points `^[0-9.-]+$` : 15 198 numéros différents
* un préfixe L, D, R, A suivi de chiffres et tirets et points `^[LDRA][0-9.-]+$` : 104 183 numéros différents
* un préfixe L, D, R, A suivi une étoile suivi de chiffres et tirets et points `^[LDRA]\*[0-9.-]+$` : 7300 numéros différents
* le préfixe LO suivi de chiffres et tirets et points `^LO[0-9.-]+$` : 1078 numéros différents

Noter qu’il n’y a aucun `^(L\.O|[LDRA])\.[0-9.-]+$` mais ces références apparaissent dans les textes, il faut donc les reconnaître et les considérer comme équivalentes à la version sans point.

Au final, la regex `^(LO|[LDRA]\*?)?[0-9.-]+$` recense 127 759 numéros différents, soit 88,9 % du total.

Lorsqu’est rajouté la possibilité de faire suivre cette regex de zéro ou plusieurs espaces puis optionnellement d’un « bis », « ter », etc (\*) : on arrive à 129 842 numéros différents, soit 90,3 % du total, il reste alors 13 925 numéros différents ayant une syntaxe plus compliquée.

(\*) regex « cies » donnée dans la grammaire comptant les ordinaux en latin de 1 à 99 : « ((un|duo|ter|quater|quin|sex|sept|octo|novo)?(dec|vic|tric|quadrag|quinquag|sexag|septuag|octog|nonag)ies|semel|bis|ter|quater|(quinqu|sex|sept|oct|non)ies) »

### Types de numérotation « compliqués »

Sur le total des 13 925 types « compliqués », il est possible de faire lentement baisser ce chiffre en composant les règles :

* ajouter des espaces éventuelles en début et fin, c’est une règle de l’ordre du « nettoyage typographique », peu influente
* ajouter un type ` *[A-Z]*`, cela capture 2561 numéros différents
* composer la règle ` *[A-Z]*` avec la règle optionnelle « cies », environ 2000 numéros différents encore
* ajouter un ou deux étoiles optionnelles en début, cela capture quelques centaines de numéros du type « \*\*R11-13 »
* ajouter un type ` *[a-z]`
* ajouter des numérotations en chiffres romains
* permettre les types « ter-0 » 
* ajouter le mot "Annexe" en début, quelque soit la casse
* ajouter le mot "Tableau" en début, quelque soit la casse
* ajouter le mot "Appendice" en début, quelque soit la casse
* ajouter les noms d’articles commençant par une ou deux lettre majuscule, mais cela introduira probablement un certain nombre de faux positifs

Ces compositions se font difficilement, voire ce n’est pas possible, avec des regex normales, l’utilisation de grammaires plus puissantes devient quasiment un pré-requis (non-testé dans cette étude, même si implémenté par la librairie metslesliens).

Une fois ces règles ajoutées, il reste de véritables exceptions (cf ci-après). Ajouter ces exceptions alourdirait la grammaire, la rendant probablement un peu ou beaucoup plus lente de façon permanente.

### Exceptions

Différentes casses peuvent apparaître (principalement en capitales ou majuscule initiale), l’idéal est probablement de permettre toutes les casses. Les listes ci-dessous ne sont pas exhaustives.

Exceptions générales :
* « Préambule »
* « Préliminaire »
* « Liminaire »
* « Unique »
* « Additionnel »
* « Additif » suivi d’une lettre majuscule

Autres types de découpage des textes :
* « CHAPITRE 1 PARAGRAPHE 1 », « CHAPITRE 1 PARAGRAPHE 2 ARTICLE 2 » (20 occurences)
* « Rubrique » suivi d’un numéro
* « Règle » suivi d’un numéro

Exceptions exceptionnelles (exemples) :
* « : CA de Besançon », « : CA de Bourges » (35 occurences, toutes les cours d’appel semble-t-il)
* « AOC " Côtes du Roussillon " » et autres AOC (235 occurences)
* « Barème A », « Barème B », « Barème de notation Femme », « Barème de notation Homme »
* « CENTRES DE FORMATION »
* « CAHIER DES CHARGES » (plusieurs occurences, parfois suivi d’un complément comme « art. 10 »)
* « CONTRAT COMMERCIAL »
* « Convention du 19 février 2009 »
* « Complément passagers » suivi d’un nombre
* « Emplois classés »
* « Français »
* « Histoire géographie et éducation »
* « ÉTATS LÉGISLATIFS ANNEXÉS »
