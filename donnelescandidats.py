#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import metslesliens

# Paramètres
if len( sys.argv ) == 3 and sys.argv[1].startswith( '--format=' ):
    format = sys.argv[1][9:]
    nomfichier = sys.argv[2]
elif len( sys.argv ) == 3 and sys.argv[2].startswith( '--format=' ):
    format = sys.argv[2][9:]
    nomfichier = sys.argv[1]
elif len( sys.argv ) == 2:
    format = 'texte'
    nomfichier = sys.argv[1]
else:
    raise Exception( 'Le nom du fichier doit être donné en argument. ')

with open( nomfichier, 'r' ) as f: texte = f.read()


# Appel à la librairie
candidats = metslesliens.donnelescandidats( texte, format )


# Résultats
if format == 'arbre':
    for candidat in candidats:
        print( 'index: ' + str( ( candidat.start, candidat.end ) ) )
        print( 'ligne: ' + str( texte.count( '\n', 0, candidat.start )+1 ) )
        print( candidat )
        print( '------------------------------------' )
elif format == 'structuré':
    for candidat in candidats:
        print( 'index: ' + str( candidat['index'] ) )
        print( 'ligne: ' + str( texte.count( '\n', 0, candidat['index'][0] )+1 ) )
        for i in candidat:
            if i != 'index':
                print( i + ': ' + str( ( candidat[i], ) )[1:-2] )
        if len( candidat ) == 1:
            print( '(données structurées manquantes)' )
        print( '------------------------------------' )
elif format == 'texte' and len( candidats ):
    lenindex = max( len( str( candidats[ len(candidats)-1 ][0] ) ), len( 'index' ) )
    lenligne = max( len( str( texte.count( '\n', 0, candidats[ len(candidats)-1 ][0] )+1 ) ), len( 'ligne' ) )
    affichage = '%%%ds %%%ds %%s' % ( lenindex, lenligne )
    print( affichage % ( 'index', 'ligne', 'texte' ) )
    for candidat in candidats:
        print( affichage % ( candidat[0], texte.count( '\n', 0, candidat[0] )+1, candidat[1] ) )
elif format == 'debug' and len( candidats ):
    lenindex = max( len( str( candidats[ len(candidats)-1 ][0] ) ), len( 'index' ) )
    lenligne = max( len( str( texte.count( '\n', 0, candidats[ len(candidats)-1 ][0] )+1 ) ), len( 'ligne' ) )
    lenavant = max( max( [ len( candidat[1] ) for candidat in candidats ] ), len( 'avant ' ) )
    affichage = '%%%ds %%%ds %%%ds%%s' % ( lenindex, lenligne, lenavant )
    print( affichage % ( 'index', 'ligne', 'avant ', 'texte' ) )
    for candidat in candidats:
        print( affichage % ( candidat[0], texte.count( '\n', 0, candidat[0] )+1, candidat[1], candidat[2] ) )

print( '> %d candidats à la ligature' % len( candidats ) )

# vim: set ts=4 sw=4 sts=4 et:
