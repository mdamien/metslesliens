from os.path import join, dirname
from setuptools import setup

def read( fname ):
    return open( join( dirname( __file__ ), fname ) ).read()

setup(
    name = "metslesliens",
    version = "1.1.1",
    author = "Seb35",
    description = "Mets des liens entre articles d’un texte de loi français.",
    license = "WTFPL",
    keywords = "law france",
    url = "https://framagit.org/parlement-ouvert/metslesliens",
    packages = [ "metslesliens" ],
    install_requires = read( "requirements.txt" ),
    long_description = read( "README.rst" ),
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: Public Domain",
        "Natural Language :: French",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Text Processing :: Linguistic",
    ],
)
