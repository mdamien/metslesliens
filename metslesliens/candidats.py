# -*- coding: utf-8 -*-
#
# Les deux fonctions publiques sont :
# - donnelesliens( texte, format = 'texte', degradations = [] )
# - generateur_donnelesliens( texte, format = 'texte', degradations = [] )
#

import os.path
import re
import parsimonious


# Constantes
longueur_debug_avant_texte = 20
longueur_debug_texte = 120

repertoire = os.path.realpath( os.path.dirname( __file__ ) )


def _lire_grammaires( func ):

    """
    Décorateur fournissant les grammaires.

    :param func:
        Fonction à décorer.
    :returns:
        Fonction donnée en argument, décorée.
    """

    with open( os.path.join( repertoire, 'grammaire-liens.txt' ), 'r' ) as f:
        texte_grammaire_articles = f.read()

    with open( os.path.join( repertoire, 'grammaire-localisation.txt' ), 'r' ) as f:
        grammaire_localisation = f.read()
    grammaire_localisation = parsimonious.Grammar( grammaire_localisation )

    with open( os.path.join( repertoire, 'localisations.txt' ), 'r' ) as f:
        f_localisations = f.read().split( '\n' )
        localisations = {}
        for l in f_localisations:
            rl = re.match( r'^([a-zA-Z_][a-zA-Z_0-9]*) *= *(.+)', l )
            if rl:
                localisations[ rl.group(1) ] = grammaire_localisation.parse( rl.group(2) ).text

    setattr( func, 'grammaires', ( texte_grammaire_articles, grammaire_localisation, localisations ) )
    setattr( func, 'grammaires_articles', {} )

    return func


def donnelescandidats( texte, format = 'texte', degradations = [] ):

    """
    Donne des expressions candidates pour être des liens dans texte de loi.

    :param texte:
        (str) Texte.
    :param format:
        ('texte'|'structuré'|'arbre') Format de résultat.
            - 'texte' = aperçu rapide notamment pour contrôle
            - 'structuré' = réutilisation dans d’autres programmes Python
            - 'arbre' = arbre capturé par Parsimonious notamment pour débuggage
    :param degradations:
        (list[str]) Liste de dégradations à appliquer sur la grammaire, parmi : 'accents', 'code du commerce'.
    :returns:
        (list[dict|(int,str)|parsimonious.Node]) Candidats de liens, ordonnés par ordre d’apparition dans le texte.
            - ((int,str)) format 'texte' : index dans le texte, à partir de 0, et expression capturée entière,
            - (dict) format 'structuré' :
                - 'index' (int, int) = index de début et fin+1
                - 'alinea' (str) = alinéa (optionnel)
                - 'article', 'chapitre', 'titre', 'livre' (list[str|(str,str)]) = liste des articles/etc, les tuples sont les plages d’articles/etc correspondant au premier et au dernier de la série (optionnel)
                - 'texte' (dict[str]) = 'nom', 'date', et 'numero' du texte (optionnel)
            - (parsimonious.Node) format 'arbre' : tout l’arbre capturé,
            - ((int,str,str)) format 'debug' : index dans le texte, à partir de 0, une vingtaine de caractères avant le pré-candidat, et 120 caractères du pré-candidat avec l’éventuelle expression capturée délimitée entre les caractères "⬤ " (noter une espace après le disque, il semble prendre l’espace de deux caractères).
    """

    return list( generateur_donnelescandidats( texte, format, degradations ) )


@_lire_grammaires
def generateur_donnelescandidats( texte, format = 'structuré', degradations = [] ):

    """
    Donne des expressions candidates pour être des liens dans texte de loi.

    :param texte:
        (str) Texte.
    :param format:
        ('texte'|'structuré'|'arbre') Format de résultat.
            - 'texte' = aperçu rapide notamment pour contrôle
            - 'structuré' = réutilisation dans d’autres programmes Python
            - 'arbre' = arbre capturé par Parsimonious notamment pour débuggage
            - 'debug' = format spécifique facilitant la recherche de faux négatifs
    :param degradations:
        (list[str]) Liste de dégradations à appliquer sur la grammaire, parmi : 'accents', 'code du commerce'.
    :returns:
        (générateur(list[dict|(int,str)|parsimonious.Node])) Candidats de liens, ordonnés par ordre d’apparition dans le texte.
            - ((int,str)) format 'texte' : index dans le texte, à partir de 0, et expression capturée entière,
            - (dict) format 'structuré' :
                - 'index' (int, int) = index de début et fin+1
                - 'alinea' (str) = alinéa (optionnel)
                - 'article', 'chapitre', 'titre', 'livre' (list[str|(str,str)]) = liste des articles/etc, les tuples sont les plages d’articles/etc correspondant au premier et au dernier de la série (optionnel)
                - 'texte' (dict[str]) = 'nom', 'date', et 'numero' du texte (optionnel)
            - (parsimonious.Node) format 'arbre' : tout l’arbre capturé,
            - ((int,str,str)) format 'debug' : index dans le texte, à partir de 0, une vingtaine de caractères avant le pré-candidat, et 120 caractères du pré-candidat avec l’éventuelle expression capturée délimitée entre les caractères "⬤ " (noter une espace après le disque, il semble prendre l’espace de deux caractères).
    """

    _, grammaire_localisation, localisations = generateur_donnelescandidats.grammaires
    grammaire_articles = _obtenir_grammaire( degradations )

    # Les pré-candidats sont les expressions commençant par les mots suivants, permettant d’accélérer le traitement
    separateurs = [ 'au', 'le', 'la', "l['’]", 'du', 'de', 'titre', 'livre', 'article', 'chapitre', 'section', 'partie' ]
    regex_precandidats = re.compile( '(?<![a-záàâäéèêëíìîïóòôöúùûüýỳŷÿ])(' + '|'.join( separateurs ) + ')', flags=re.IGNORECASE )

    # Les candidats sont reconnus par la grammaire
    candidat = None
    avancement = 0
    for precandidat in regex_precandidats.finditer( texte ):

        # Ne pas re-capturer une partie d’expression déjà capturée
        if precandidat.start() < avancement:
            continue

        arbre = None
        try:
            arbre = grammaire_articles.match( texte, precandidat.start() )

        except parsimonious.exceptions.ParseError:

            if format == 'debug':
                debut_texte = texte[ precandidat.start() - longueur_debug_avant_texte : precandidat.start() ].rsplit( '\n', 1 )[-1]
                fin_texte = texte[ precandidat.start() : precandidat.start() + longueur_debug_texte + 4 ].split( '\n', 1 )[0]
                yield ( precandidat.start(), debut_texte, fin_texte )

            continue

        avancement = precandidat.start() + len( arbre.text )

        if format == 'arbre':

            yield arbre

        elif format == 'texte':

            yield ( precandidat.start(), arbre.text )

        elif format == 'structuré':

            # Récupérer les données intéressantes, cf la grammaire localisations.txt
            donnees = {}
            for donnee in localisations.keys():
                donnees[donnee] = _obtenir_texte_depuis_un_arbre_regule( localisations[donnee], arbre, grammaire_localisation )

            candidat = { 'index': ( arbre.start, arbre.end ) }

            if donnees['alinea']:
                candidat['alinea'] = donnees['alinea']

            if donnees['article']:
                candidat['article'] = [ donnees['article'] ]
                donnees['articles'] = _obtenir_texte_depuis_un_arbre_regule( localisations['articles'], arbre, grammaire_localisation, format = 'arbre' )
                if donnees['articles']:
                    for i in range( 0, len( donnees['articles'].children ) ):
                        l = len( candidat['article'] )
                        arbre_tronque = donnees['articles'].children[i].children[1].children[0]
                        if donnees['articles'].children[i].children[0].children[0].expr_name == 'plage':
                            candidat['article'][l-1] = ( candidat['article'][l-1], arbre_tronque.text )
                        else:
                            candidat['article'].append( arbre_tronque.text )

            if donnees['type_partie'] and donnees['numero_partie']:
                type_partie = donnees['type_partie'].lower()
                candidat[type_partie] = [ donnees['numero_partie'] ]
                donnees['numeros_partie'] = _obtenir_texte_depuis_un_arbre_regule( localisations['numeros_partie'], arbre, grammaire_localisation, format = 'arbre' )
                if donnees['numeros_partie']:
                    for i in range( 0, len( donnees['numeros_partie'].children ) ):
                        l = len( candidat[type_partie] )
                        arbre_tronque = donnees['numeros_partie'].children[i].children[1].children[0]
                        if donnees['numeros_partie'].children[i].children[0].children[0].expr_name == 'plage':
                            candidat[type_partie][l-1] = ( candidat[type_partie][l-1], arbre_tronque.text )
                        else:
                            candidat[type_partie].append( arbre_tronque.text )

            if donnees['type_relatif'] and donnees['adjectif_relatif']:
                type_relatif = donnees['type_relatif'].lower()
                candidat[type_relatif] = [ donnees['adjectif_relatif'] ]

            if donnees['nom_texte']:
                candidat['texte'] = { 'nom': donnees['nom_texte'], 'numero': donnees['numero_texte'] or '', 'date': donnees['date_texte'] or '' }

            yield candidat

        elif format == 'debug':

            debut_texte = texte[ precandidat.start() - longueur_debug_avant_texte : precandidat.start() ].rsplit( '\n', 1 )[-1]
            fin_texte = texte[ precandidat.start() + len( arbre.text ) : precandidat.start() + longueur_debug_texte ].split( '\n', 1 )[0]

            yield ( precandidat.start(), debut_texte, '⬤ ' + arbre.text + '⬤ ' + fin_texte )


def _obtenir_grammaire( degradations ):

    """
    Revoit une grammaire avec des règles légèrement dégradées selon certaines typologies d’erreurs.

    :param degradations:
        (list[str]) Liste de dégradations à appliquer sur la grammaire, parmi : 'accents', 'code du commerce'.
    :returns:
        (parsimonious.Grammar) Grammaire.
    """

    degradations.sort()
    cle_degradations = '|'.join( degradations )

    if cle_degradations in generateur_donnelescandidats.grammaires_articles:
        return generateur_donnelescandidats.grammaires_articles[ cle_degradations ]

    texte_grammaire_articles, _, _ = generateur_donnelescandidats.grammaires

    if 'accents' in degradations:
        texte_grammaire_articles = re.sub( r'[éèê]', '[éèêëe]', texte_grammaire_articles )
        texte_grammaire_articles = texte_grammaire_articles.replace( 'à', '[áàâäa]' )
        texte_grammaire_articles = texte_grammaire_articles.replace( 'ô', '[óòôöo]' )
        texte_grammaire_articles = texte_grammaire_articles.replace( 'û', '[úùûüu]' )
        texte_grammaire_articles = texte_grammaire_articles.replace( '[a-zá[áàâäa]âä[éèêëe][éèêëe][éèêëe]ëíìîïóò[óòôöo]öúù[úùûüu]üýỳŷÿ]', '[a-záàâäéèêëíìîïóòôöúùûüýỳŷÿ]' )

    if 'code du commerce' in degradations:
        texte_grammaire_articles = texte_grammaire_articles.replace( 'de +commerce', 'd[eu] +commerce' )

    generateur_donnelescandidats.grammaires_articles[ cle_degradations ] = parsimonious.Grammar( texte_grammaire_articles )

    return generateur_donnelescandidats.grammaires_articles[ cle_degradations ]


def _obtenir_texte_depuis_un_arbre_regule( regle, arbre, grammaire, format = 'texte' ):

    """
    Obtenir une feuille d’un arbre à partir d’une règle décrivant sa localisation.

    :param regle:
        (str) Règle.
    :param arbre:
        (parsimonious.Node) Arbre.
    :param grammaire:
        (parsimonious.Grammar) Grammaire de lecture des règles.
    :param format:
        (str) Format de sortie : 'texte' ou 'arbre'.
    :returns:
        (str|parsimonious.Node|None) Feuille ou nœud de l’arbre localisée par la règle, None si absent.
    """

    trouve = False

    try:
        arbre_grammaire = grammaire.parse( regle )
    except parsimonious.exceptions.ParseError:
        return None

    for i in range( 0, len( arbre_grammaire.children[0].children ) ):
        arbre_tronque = arbre
        trouve = False
        for j in range( 0, len( arbre_grammaire.children[0].children[i].children[0].children ) ):
            identifiant = arbre_grammaire.children[0].children[i].children[0].children[j].children[0].children[0]
            trouve = False
            if identifiant.expr_name == 'numero' and int( identifiant.text ) < len( arbre_tronque.children ):
                arbre_tronque = arbre_tronque.children[ int( identifiant.text ) ]
                trouve = True
            elif identifiant.expr_name == 'libelle':
                for sousarbre in arbre_tronque.children:
                    if sousarbre.expr_name == identifiant.text:
                        arbre_tronque = sousarbre
                        trouve = True
                        break
            if not trouve:
                break
        if trouve:
            break

    if not trouve:
        return None

    if format == 'arbre':
        return arbre_tronque

    return arbre_tronque.text

# vim: set ts=4 sw=4 sts=4 et:
